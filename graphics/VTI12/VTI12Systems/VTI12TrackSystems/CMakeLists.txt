# $Id: CMakeLists.txt 728682 2016-03-09 15:17:26Z krasznaa $
################################################################################
# Package: VTI12TrackSystems
################################################################################

# Declare the package name:
atlas_subdir( VTI12TrackSystems )

# Declare the package's dependencies:
atlas_depends_on_subdirs(
   PUBLIC
   DetectorDescription/GeoPrimitives
   DetectorDescription/Identifier
#   Event/xAOD/xAODTracking
   GaudiKernel
   Generators/GeneratorObjects
   Simulation/G4Sim/TrackRecord
   Tracking/TrkDetDescr/TrkDetDescrInterfaces
   Tracking/TrkDetDescr/TrkSurfaces
   Tracking/TrkEvent/TrkEventPrimitives
   Tracking/TrkEvent/TrkParameters
   graphics/VP1/VP1Base
   graphics/VTI12/VTI12Utils
   PRIVATE
   Control/AthContainers
   Control/StoreGate
   DetectorDescription/FaserDetDescr
   Event/EventPrimitives
   Tracker/TrackerDetDescr/TrackerIdentifier
   Tracker/TrackerDetDescr/TrackerReadoutGeometry
#   InnerDetector/InDetRecEvent/InDetPrepRawData
#   InnerDetector/InDetRecEvent/InDetRIO_OnTrack
   Tracker/TrackerSimEvent
   Scintillator/ScintDetDescr/ScintIdentifier
   Scintillator/ScintDetDescr/ScintReadoutGeometry
   Scintillator/ScintSimEvent
   Reconstruction/Particle
   Tracking/TrkDetDescr/TrkDetDescrUtils
   Tracking/TrkDetDescr/TrkDetElementBase
   Tracking/TrkDetDescr/TrkVolumes
   Tracking/TrkEvent/TrkCompetingRIOsOnTrack
   Tracking/TrkEvent/TrkMaterialOnTrack
   Tracking/TrkEvent/TrkMeasurementBase
   Tracking/TrkEvent/TrkPrepRawData
   Tracking/TrkEvent/TrkPseudoMeasurementOnTrack
   Tracking/TrkEvent/TrkRIO_OnTrack
   Tracking/TrkEvent/TrkSegment
   Tracking/TrkEvent/TrkTrack
   Tracking/TrkEvent/TrkTrackSummary
   Tracking/TrkExtrapolation/TrkExInterfaces
   Tracking/TrkFitter/TrkFitterInterfaces
   graphics/VP1/VP1HEPVis
   graphics/VTI12/VTI12Systems/VTI12GuideLineSystems
#   graphics/VP1/VP1Systems/VP1PRDSystems 
   )

# External dependencies:
find_package( CLHEP )
find_package( Coin3D )
find_package( HepMC )
find_package( Qt5 COMPONENTS Core Gui Widgets  )
find_package( GeoModelCore )

# Generate UI files automatically:
# Note: add the "Widgets" component to "find_package( Qt5 ...)" if you have UI files, otherwise UIC, even if CMAKE_AUTOUIC is set to ON, is not run
set( CMAKE_AUTOUIC TRUE )
# Generate MOC files automatically:
set( CMAKE_AUTOMOC TRUE )

# Component(s) in the package:
atlas_add_library( VTI12TrackSystems VTI12TrackSystems/*.h src/*.cxx
   PUBLIC_HEADERS VTI12TrackSystems
   PRIVATE_INCLUDE_DIRS ${COIN3D_INCLUDE_DIRS} ${CLHEP_INCLUDE_DIRS}
   ${HEPMC_INCLUDE_DIRS} ${CMAKE_CURRENT_BINARY_DIR}
   LINK_LIBRARIES ${GEOMODEL_LIBRARIES} GeoPrimitives Identifier 
   #xAODTracking
   GaudiKernel GeneratorObjects TrkDetDescrInterfaces TrkSurfaces
   TrkEventPrimitives TrkParameters VP1Base VTI12Utils StoreGateLib SGtests
   Qt5::Core Qt5::Gui
   PRIVATE_LINK_LIBRARIES ${COIN3D_LIBRARIES} ${CLHEP_LIBRARIES}
   ${HEPMC_LIBRARIES} AthContainers FaserDetDescr EventPrimitives
   ScintIdentifier ScintSimEvent ScintReadoutGeometry
   TrackerIdentifier TrackerReadoutGeometry TrackerSimEvent
   #InDetPrepRawData InDetRIO_OnTrack
   Particle
   TrkDetDescrUtils TrkDetElementBase TrkVolumes TrkCompetingRIOsOnTrack
   TrkMaterialOnTrack TrkMeasurementBase TrkPrepRawData
   TrkPseudoMeasurementOnTrack TrkRIO_OnTrack TrkSegment TrkTrack
   TrkTrackSummary TrkExInterfaces TrkFitterInterfaces VP1HEPVis
   VP1GuideLineSystems
   #VP1PRDSystems 
   )

################################################################################
# Package: VTI12Utils
################################################################################
# Author: Thomas Kittelmann
# Author: Riccardo Maria BIANCHI <rbianchi@cern.ch>
################################################################################

# Declare the package name:
atlas_subdir( VTI12Utils )

# Declare the package's dependencies:
atlas_depends_on_subdirs(
   PUBLIC
   Control/AthenaKernel
   Control/StoreGate
   DetectorDescription/GeoPrimitives
   Event/EventPrimitives
   GaudiKernel
   graphics/VP1/VP1Base
   PRIVATE
   Control/CxxUtils
   DetectorDescription/FaserDetDescr
   DetectorDescription/GeoModel/GeoModelUtilities
   DetectorDescription/GeoModel/GeoModelFaserUtilities
   DetectorDescription/GeoModel/GeoSpecialShapes
   DetectorDescription/Identifier
   Scintillator/ScintDetDescr/ScintIdentifier
   Scintillator/ScintDetDescr/ScintReadoutGeometry
   Tracker/TrackerDetDescr/TrackerIdentifier
   Tracker/TrackerDetDescr/TrackerReadoutGeometry
#   InnerDetector/InDetRecEvent/InDetRIO_OnTrack
   Tracking/TrkDetDescr/TrkSurfaces
   Tracking/TrkEvent/TrkRIO_OnTrack
   graphics/VP1/VP1HEPVis )

# External dependencies:
find_package( CLHEP ) # TODO: to be removed when fully migrated to Eigen-based GeoTrf
find_package( Coin3D )
find_package( Eigen )
find_package( HepPDT )
find_package( Qt5 COMPONENTS Core REQUIRED)
find_package( GeoModelCore )

# Generate MOC files automatically:
set( CMAKE_AUTOMOC ON )

# Component(s) in the package:
atlas_add_library( VTI12Utils VTI12Utils/*.h src/*.cxx src/*.cpp
   PUBLIC_HEADERS VTI12Utils
   INCLUDE_DIRS ${CLHEP_INCLUDE_DIRS}
   PRIVATE_INCLUDE_DIRS ${HEPPDT_INCLUDE_DIRS} ${COIN3D_INCLUDE_DIRS}
   ${EIGEN_INCLUDE_DIRS}
   DEFINITIONS ${CLHEP_DEFINITIONS}
   LINK_LIBRARIES ${CLHEP_LIBRARIES} ${GEOMODEL_LIBRARIES} EventPrimitives
   GaudiKernel VP1Base StoreGateLib SGtests AthDSoCallBacks 
   GeoPrimitives Qt5::Core
   PRIVATE_LINK_LIBRARIES ${HEPPDT_LIBRARIES} ${COIN3D_LIBRARIES}
   ${EIGEN_LIBRARIES} CxxUtils FaserDetDescr
   GeoModelUtilities GeoModelFaserUtilities GeoSpecialShapes Identifier
   ScintIdentifier ScintReadoutGeometry
   TrackerIdentifier TrackerReadoutGeometry
   #InDetRIO_OnTrack 
   TrkSurfaces TrkRIO_OnTrack VP1HEPVis )

################################################################################
# Package: GeoModelTest
################################################################################

# Declare the package name:
atlas_subdir( GeoModelTest )

# Declare the package's dependencies:
atlas_depends_on_subdirs( PRIVATE
                          Control/AthenaBaseComps
                          DetectorDescription/GeoModel/GeoModelInterfaces
                          DetectorDescription/GeoModel/GeoModelUtilities
                          Scintillator/ScintDetDescr/ScintReadoutGeometry
                          Tracker/TrackerDetDescr/TrackerReadoutGeometry
                          MagneticField/MagFieldInterfaces
                          Tracker/TrackerAlignTools/TrackerAlignGenTools
                        )

# External dependencies:
find_package( GeoModelCore )

# Component(s) in the package:
atlas_add_component( GeoModelTest
                     src/GeoModelTestAlg.cxx
                     src/components/GeoModelTest_entries.cxx
                     INCLUDE_DIRS ${GEOMODEL_INCLUDE_DIRS}
                     LINK_LIBRARIES ${GEOMODEL_LIBRARIES} AthenaBaseComps GeoModelUtilities ScintReadoutGeometry TrackerReadoutGeometry MagFieldInterfaces )

atlas_add_test( GeoModelCheck
                SCRIPT python/GeoModelTestConfig.py
                PROPERTIES WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
                PROPERTIES TIMEOUT 300 )

# Install files from the package:
#atlas_install_headers( GeoModelTest )
#atlas_install_joboptions( share/*.py )
atlas_install_python_modules( python/*.py )
atlas_install_scripts( scripts/*.sh )

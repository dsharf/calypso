# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

from __future__ import print_function

from AthenaConfiguration.AthConfigFlags import AthConfigFlags

# This module is based upon Control/AthenaCommon/python/DetFlags.py
# Only some flags have been migrated. 

def createDetectorConfigFlags():
    dcf=AthConfigFlags()

    #Detector.Geometry - merger of the old geometry and detdescr tasks
    dcf.addFlag('Detector.GeometryUpstreamDipole',  False)
    dcf.addFlag('Detector.GeometryCentralDipole',   False)
    dcf.addFlag('Detector.GeometryDownstreamDipole',False)
    dcf.addFlag('Detector.GeometryDipole',          lambda prevFlags : (prevFlags.Detector.GeometryUpstreamDipole or
                                                                        prevFlags.Detector.GeometryCentralDipole or
                                                                        prevFlags.Detector.GeometryDownstreamDipole))
    dcf.addFlag('Detector.GeometryVeto',            False)
    dcf.addFlag('Detector.GeometryTrigger',         False)
    dcf.addFlag('Detector.GeometryPreshower',       False)
    dcf.addFlag('Detector.GeometryScintillator',    lambda prevFlags : (prevFlags.Detector.GeometryVeto or
                                                                        prevFlags.Detector.GeometryTrigger or
                                                                        prevFlags.Detector.GeometryPreshower))
    dcf.addFlag('Detector.GeometryFaserSCT',        False)
    dcf.addFlag('Detector.GeometryTracker',         lambda prevFlags : prevFlags.Detector.GeometryFaserSCT )
    dcf.addFlag('Detector.GeometryEcal',            False)
    dcf.addFlag('Detector.GeometryFaserCalo',       lambda prevFlags : prevFlags.DetectorGeometry.Ecal)
    dcf.addFlag('Detector.GeometryFaser',           lambda prevFlags : (prevFlags.Detector.GeometryDecayVolume or
                                                                        prevFlags.Detector.GeometryScintillator or
                                                                        prevFlags.Detector.GeometryTracker or
                                                                        prevFlags.Detector.GeometryFaserCalo))

    #Detector.Simulate
    dcf.addFlag('Detector.SimulateUpstreamDipole',  False)
    dcf.addFlag('Detector.SimulateCentralDipole',   False)
    dcf.addFlag('Detector.SimulateDownstreamDipole',False)
    dcf.addFlag('Detector.SimulateDipole',          lambda prevFlags : (prevFlags.Detector.SimulateUpstreamDipole or
                                                                        prevFlags.Detector.SimulateCentralDipole or
                                                                        prevFlags.Detector.SimulateDownstreamDipole))
    dcf.addFlag('Detector.SimulateDecayVolume', False)
    dcf.addFlag('Detector.SimulateVeto',        False)
    dcf.addFlag('Detector.SimulateTrigger',     False)
    dcf.addFlag('Detector.SimulatePreshower',   False)
    dcf.addFlag('Detector.SimulateScintillator',lambda prevFlags : (prevFlags.Detector.SimulateVeto or
                                                                    prevFlags.Detector.SimulateTrigger or
                                                                    prevFlags.Detector.SimulatePreshower))
    dcf.addFlag('Detector.SimulateFaserSCT',    False)
    dcf.addFlag('Detector.SimulateTracker',     lambda prevFlags : prevFlags.Detector.SimulateFaserSCT )
    dcf.addFlag('Detector.SimulateEcal',        False)
    dcf.addFlag('Detector.SimulateFaserCalo',   lambda prevFlags : prevFlags.Detector.SimulateEcal)
    dcf.addFlag('Detector.SimulateFaser',       lambda prevFlags : (prevFlags.Detector.SimulateDecayVolume or
                                                                    prevFlags.Detector.SimulateScintillator or 
                                                                    prevFlags.Detector.SimulateTracker or
                                                                    prevFlags.Detector.SimulateFaserCalo))

    #Detector.Overlay
    dcf.addFlag('Detector.OverlayVeto',         False)
    dcf.addFlag('Detector.OverlayTrigger',      False)
    dcf.addFlag('Detector.OverlayPreshower',    False)
    dcf.addFlag('Detector.OverlayScintillator', lambda prevFlags : (prevFlags.Detector.OverlayVeto or
                                                                    prevFlags.Detector.OverlayTrigger or
                                                                    prevFlags.Detector.OverlayPreshower))
    dcf.addFlag('Detector.OverlayFaserSCT',     False)
    dcf.addFlag('Detector.OverlayTracker',      lambda prevFlags : prevFlags.Detector.OverlayFaserSCT )
    dcf.addFlag('Detector.OverlayEcal',         False)
    dcf.addFlag('Detector.OverlayFaserCalo',    lambda prevFlags : prevFlags.Detector.OverlayEcal)
    dcf.addFlag('Detector.OverlayFaser',        lambda prevFlags : (prevFlags.Detector.OverlayScintillator or 
                                                                    prevFlags.Detector.OverlayTracker or
                                                                    prevFlags.Detector.OverlayFaserCalo))

    return dcf

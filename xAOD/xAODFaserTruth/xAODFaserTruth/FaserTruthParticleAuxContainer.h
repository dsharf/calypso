#ifndef XAODFASERTRUTH_TRUTHPARTICLEAUXCONTAINER_H
#define XAODFASERTRUTH_TRUTHPARTICLEAUXCONTAINER_H

#include <vector>

#include "AthLinks/ElementLink.h"
#include "xAODCore/AuxContainerBase.h"

#include "xAODFaserTruth/FaserTruthParticleContainer.h"
#include "xAODFaserTruth/FaserTruthVertexContainer.h"

namespace xAOD {


  /// Auxiliary store for the truth vertices
  ///
  /// @author Andy Buckley <Andy.Buckey@cern.ch>
  /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
  /// @author Jovan Mitrevski <Jovan.Mitrevski@cern.h>
  ///
  class FaserTruthParticleAuxContainer : public AuxContainerBase {

  public:
    /// Default constructor
    FaserTruthParticleAuxContainer();

  private:
    std::vector< int > pdgId;
    std::vector< int > barcode;
    std::vector< int > status;
    std::vector< ElementLink< FaserTruthVertexContainer > > prodVtxLink;
    std::vector< ElementLink< FaserTruthVertexContainer > > decayVtxLink;
    std::vector< float > px;
    std::vector< float > py;
    std::vector< float > pz;
    std::vector< float > e;
    std::vector< float > m; // needed since not necessarily on shell

  }; // class FaserTruthParticleAuxContainer

} // namespace xAOD

// Declare a CLID for the class
#include "xAODCore/CLASS_DEF.h"
CLASS_DEF( xAOD::FaserTruthParticleAuxContainer, 1107850896, 1 )

#endif // XAODFASERTRUTH_TRUTHPARTICLEAUXCONTAINER_H

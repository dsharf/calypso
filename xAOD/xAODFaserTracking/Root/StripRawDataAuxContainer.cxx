#include "xAODFaserTracking/StripRawDataAuxContainer.h"

namespace xAOD {
  StripRawDataAuxContainer::StripRawDataAuxContainer()
    : AuxContainerBase()
  {
    AUX_VARIABLE(id);
    AUX_VARIABLE(dataword);
  }
  
    void StripRawDataAuxContainer::dump() const {
    std::cout<<" Dumping StripRawDataAuxContainer"<<std::endl;
    std::cout<<"id:";
    std::copy(id.begin(), id.end(),
        std::ostream_iterator<float>(std::cout, ", "));
    std::cout<<"dataword:";
    std::copy(dataword.begin(), dataword.end(),
        std::ostream_iterator<float>(std::cout, ", "));
        std::cout<<std::endl;
  }
}
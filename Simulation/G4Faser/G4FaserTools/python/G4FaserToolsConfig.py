# Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration

from AthenaCommon import CfgMgr

def generateFastSimulationList():
    FastSimulationList=[]
    #
    # Not yet supported in FASER
    #
    # from G4AtlasApps.SimFlags import simFlags
    # from CalypsoCommon.DetFlags import DetFlags
    # if DetFlags.bpipe_on():
    #     if hasattr(simFlags, 'ForwardDetectors') and simFlags.ForwardDetectors.statusOn and simFlags.ForwardDetectors() == 2:
    #         FastSimulationList += ['ForwardTransportModel']
    #     if hasattr(simFlags, 'BeamPipeSimMode') and simFlags.BeamPipeSimMode.statusOn and simFlags.BeamPipeSimMode() != "Normal":
    #         FastSimulationList += [ 'SimpleFastKiller' ]
    # if DetFlags.geometry.LAr_on():
    #     ## Shower parameterization overrides the calibration hit flag
    #     if simFlags.LArParameterization.statusOn and simFlags.LArParameterization() > 0 \
    #             and simFlags.CalibrationRun.statusOn and simFlags.CalibrationRun.get_Value() in ['LAr','LAr+Tile','DeadLAr']:
    #         print 'getFastSimulationMasterTool FATAL :: You requested both calibration hits and frozen showers / parameterization in the LAr.'
    #         print '  Such a configuration is not allowed, and would give junk calibration hits where the showers are modified.'
    #         print '  Please try again with a different value of simFlags.LArParameterization or simFlags.CalibrationRun '
    #         raise RuntimeError('Configuration not allowed')
    #     if simFlags.LArParameterization() > 0:
    #         #FIXME If we're only using Frozen Showers in the FCAL do we really need to set up the EMB and EMEC as well?
    #         FastSimulationList += ['EMBFastShower', 'EMECFastShower', 'FCALFastShower', 'FCAL2FastShower']
    #         if simFlags.LArParameterization.get_Value() > 1:
    #              FastSimulationList += ['DeadMaterialShower']
    #     elif simFlags.LArParameterization() is None or simFlags.LArParameterization() == 0:
    #         print "getFastSimulationMasterTool INFO No Frozen Showers"
    # if DetFlags.Muon_on():
    #     if hasattr(simFlags, 'CavernBG') and simFlags.CavernBG.statusOn and simFlags.CavernBG.get_Value() != 'Read' and\
    #             not (hasattr(simFlags, 'RecordFlux') and simFlags.RecordFlux.statusOn and simFlags.RecordFlux()):
    #         FastSimulationList += ['NeutronFastSim']
    return FastSimulationList

# def getFastSimulationMasterTool(name="FastSimulationMasterTool", **kwargs):
#     kwargs.setdefault("FastSimulations", generateFastSimulationList())
#     return CfgMgr.FastSimulationMasterTool(name, **kwargs)

# def getEmptyFastSimulationMasterTool(name="EmptyFastSimulationMasterTool", **kwargs):
#     return CfgMgr.FastSimulationMasterTool(name, **kwargs)

# def generateFwdSensitiveDetectorList():
#     SensitiveDetectorList=[]
#     from G4AtlasApps.SimFlags import simFlags
#     from AthenaCommon.DetFlags import DetFlags
#     if DetFlags.simulate.Lucid_on():
#         SensitiveDetectorList += [ 'LUCID_SensitiveDetector' ]
#     if hasattr(simFlags, 'ForwardDetectors') and simFlags.ForwardDetectors.statusOn:
#         if DetFlags.simulate.ZDC_on():
#             SensitiveDetectorList += [ 'ZDC_PixelSD', 'ZDC_StripSD' ]
#         if DetFlags.simulate.ALFA_on():
#             SensitiveDetectorList += [ 'ALFA_SensitiveDetector' ]
#         if DetFlags.simulate.AFP_on():
#             SensitiveDetectorList += [ 'AFP_SensitiveDetector' ]
#             #SensitiveDetectorList += [ 'AFP_SiDSensitiveDetector', 'AFP_TDSensitiveDetector' ]

#     return SensitiveDetectorList

# def generateTrackFastSimSensitiveDetectorList():
#     SensitiveDetectorList=[]
#     from AthenaCommon.DetFlags import DetFlags
#     from G4AtlasApps.SimFlags import simFlags
#     if (DetFlags.Muon_on() and simFlags.CavernBG.statusOn and simFlags.CavernBG.get_Value() != 'Read' and 'Write' in simFlags.CavernBG.get_Value()) or (hasattr(simFlags, 'StoppedParticleFile') and simFlags.StoppedParticleFile.statusOn):
#         SensitiveDetectorList += [ 'TrackFastSimSD' ]
#     return SensitiveDetectorList

def generateScintillatorSensitiveDetectorList():
    SensitiveDetectorList=[]
    from CalypsoConfiguration.AllConfigFlags import ConfigFlags
    if (ConfigFlags.Detector.SimulateScintillator):
        if (ConfigFlags.Detector.SimulateVeto):
            SensitiveDetectorList += [ 'VetoSensorSD' ]
        # if (ConfigFlags.Detector.SimulateTrigger):
        #     SensitiveDetectorList += [ 'TriggerSensorSD']
        # if (ConfigFlags.Detector.SimulatePreshowe):
        #     SensitiveDetectorList += [ 'PreshowerSD']
    return SensitiveDetectorList

def generateTrackerSensitiveDetectorList():
    SensitiveDetectorList=[]
    from CalypsoConfiguration.AllFlags import ConfigFlags
    if (ConfigFlags.Detector.SimulateTracker):
        if (ConfigFlags.Detector.SimulateFaserSCT):
            SensitiveDetectorList += [ 'SctSensorSD' ]
    return SensitiveDetectorList

def generateFaserCaloSensitiveDetectorList():
    SensitiveDetectorList=[]
    # from CalypsoConfiguration.AllFlags import ConfigFlags
    # if (ConfigFlags.Detector.SimulateFaserCalo):
    #     SensitiveDetectorList += [ 'FaserCaloSensorSD' ]
    return SensitiveDetectorList

def generateSensitiveDetectorList():
    SensitiveDetectorList=[]
    SensitiveDetectorList += generateScintillatorSensitiveDetectorList()
    SensitiveDetectorList += generateTrackerSensitiveDetectorList()
    SensitiveDetectorList += generateFaserCaloSensitiveDetectorList()
    return SensitiveDetectorList

def getSensitiveDetectorMasterTool(name="SensitiveDetectorMasterTool", **kwargs):
    kwargs.setdefault("SensitiveDetectors", generateSensitiveDetectorList())
    return CfgMgr.SensitiveDetectorMasterTool(name, **kwargs)

def getEmptySensitiveDetectorMasterTool(name="EmptySensitiveDetectorMasterTool", **kwargs):
    return CfgMgr.SensitiveDetectorMasterTool(name, **kwargs)

def getPhysicsListToolBase(name="PhysicsListToolBase", **kwargs):
    PhysOptionList = ["G4StepLimitationTool"]
    from CalypsoConfiguration.AllFlags import ConfigFlags
    PhysOptionList += ConfigFlags.Sim.PhysicsOptions
    PhysDecaysList = []

    kwargs.setdefault("PhysOption", PhysOptionList)
    kwargs.setdefault("PhysicsDecay", PhysDecaysList)
    kwargs.setdefault("PhysicsList", ConfigFlags.Sim.PhysicsList)

    if 'PhysicsList' in kwargs:
        if kwargs['PhysicsList'].endswith('_EMV') or kwargs['PhysicsList'].endswith('_EMX'):
            raise RuntimeError( 'PhysicsList not allowed: '+kwargs['PhysicsList'] )
    kwargs.setdefault("GeneralCut", 1.)
    if (ConfigFlags.Sim.NeutronTimeCut.statusOn):
        kwargs.setdefault("NeutronTimeCut", ConfigFlags.Sim.NeutronTimeCut)
    if (ConfigFlags.Sim.NeutronEnergyCut.statusOn):
        kwargs.setdefault("NeutronEnergyCut", ConfigFlags.Sim.NeutronEnergyCut)
    if (ConfigFlags.Sim.ApplyEMCuts.statusOn):
        kwargs.setdefault("ApplyEMCuts", ConfigFlags.Sim.ApplyEMCuts)
    return CfgMgr.PhysicsListToolBase(name, **kwargs)
    
def getReducedStepSizeUserLimitsTool(name="ReducedStepSizeUserLimitsTool", **kwargs):
    from AthenaCommon.SystemOfUnits import millimeter
    kwargs.setdefault("OutputLevel", 1)
    kwargs.setdefault("VolumeList", [
                                    "Faser::Faser",
                                    ])
    kwargs.setdefault("MaxStep", 10.*millimeter)
    kwargs.setdefault("MatchType", "contains")
    return CfgMgr.UserLimitsTool(name, **kwargs)


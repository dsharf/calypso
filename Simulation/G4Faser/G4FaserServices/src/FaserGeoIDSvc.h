/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

//////////////////////////////////////////////////////////////////
// FaserGeoIDSvc.h, (c) ATLAS Detector software
///////////////////////////////////////////////////////////////////

#ifndef G4FASERSERVICES_FASERGEOIDSVC_H
#define G4FASERSERVICES_FASERGEOIDSVC_H 1

// framework includes
#include "GaudiKernel/ServiceHandle.h"
#include "AthenaBaseComps/AthService.h"

// STL includes
#include <vector>
#include <list>
#include <set>

// DetectorDescription
#include "AtlasDetDescr/AtlasRegion.h"

// ISF includes
#include "ISF_Interfaces/IGeoIDSvc.h"

namespace ISF {

  /** @class GeoIDSvc
  
      A fast Athena service identifying the AtlasRegion a given position/particle is in.

      @author Elmar.Ritsch -at- cern.ch

      This dummy version for FASER says that everything is in the "undefined" ATLAS region
  
     */
  class FaserGeoIDSvc : public extends<AthService, ISF::IGeoIDSvc> {
    public: 
     /** Constructor with parameters */
     FaserGeoIDSvc(const std::string& name,ISvcLocator* svc);

     /** Destructor */
     ~FaserGeoIDSvc();

     // Athena algtool's Hooks
     StatusCode  initialize();
     StatusCode  finalize();

     /** A static filter that returns the SimGeoID of the given position */
     AtlasDetDescr::AtlasRegion    identifyGeoID(const Amg::Vector3D &pos) const;

     /** Checks if the given position (or ISFParticle) is inside a given SimGeoID */
     ISF::InsideType inside(const Amg::Vector3D &pos, AtlasDetDescr::AtlasRegion geoID) const;

     /** Find the SimGeoID that the particle will enter with its next infinitesimal step
         along the given direction */
     AtlasDetDescr::AtlasRegion identifyNextGeoID(const Amg::Vector3D &pos, const Amg::Vector3D &dir) const;

    private:

   }; 
  
} // ISF namespace

#endif //> !FASERSERVICES_FASERGEOIDSVC_H

/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

///////////////////////////////////////////////////////////////////
// GeoIDSvc.cxx, (c) ATLAS Detector software
///////////////////////////////////////////////////////////////////

// class header include
#include "FaserGeoIDSvc.h"

// framework includes
#include "GaudiKernel/Bootstrap.h"
#include "GaudiKernel/ISvcLocator.h"

// DetectorDescription
#include "AtlasDetDescr/AtlasRegionHelper.h"

// STL includes
#include <algorithm>

/** Constructor **/
ISF::FaserGeoIDSvc::FaserGeoIDSvc(const std::string& name,ISvcLocator* svc) :
  base_class(name,svc)
{ }


/** Destructor **/
ISF::FaserGeoIDSvc::~FaserGeoIDSvc()
{ }


// Athena algtool's Hooks
StatusCode  ISF::FaserGeoIDSvc::initialize()
{
  ATH_MSG_INFO("initialize() ...");

   ATH_MSG_INFO("initialize() successful");
  return StatusCode::SUCCESS;
}


StatusCode  ISF::FaserGeoIDSvc::finalize() {
  ATH_MSG_INFO("finalize() ...");

  ATH_MSG_INFO("finalize() successful");
  return StatusCode::SUCCESS;
}


ISF::InsideType ISF::FaserGeoIDSvc::inside(const Amg::Vector3D& , AtlasDetDescr::AtlasRegion geoID) const {

// Only one answer...
  return (geoID == AtlasDetDescr::fUndefinedAtlasRegion ? ISF::fInside : ISF::fOutside);
}


AtlasDetDescr::AtlasRegion ISF::FaserGeoIDSvc::identifyGeoID(const Amg::Vector3D& ) const {

// Only one answer...
  return AtlasDetDescr::fUndefinedAtlasRegion;
}


AtlasDetDescr::AtlasRegion ISF::FaserGeoIDSvc::identifyNextGeoID(const Amg::Vector3D& ,
                                                            const Amg::Vector3D& ) const {

// Only one answer...
  return AtlasDetDescr::fUndefinedAtlasRegion;
}

